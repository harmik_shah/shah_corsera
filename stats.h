/******************************************************************************
 * Copyright (C) 2017 by Alex Fosdick - University of Colorado
 *
 * Redistribution, modification or use of this software in source or binary
 * forms is permitted as long as the files maintain this copyright. Users are 
 * permitted to modify this and use it to learn about the field of embedded
 * software. Alex Fosdick and the University of Colorado are not liable for any
 * misuse of this material. 
 *
 *****************************************************************************/
/**
 * @file stats.c 
 * @brief To perform various operations on an array. 
 
 * To write a few functions that can analyze an array of unsigned char data ite * ms and report analytics on the maximum, minimum, mean, and meidan of the dat * a set.
 *
 * @author Harmik Shah
 * @date 13-04-2020
 *
 */
#ifndef __STATS_H__
#define __STATS_H__

int find_mean(unsigned char test[] , int count); 

/**
 * @brief Function to find the mean of the array
 *
 * The function takes the array and count of number of elements
 * as input and then adds all the elements of the average through
 * the for loop and then we get the mean by dividing the sum by 
 * number of elements.
 *
 * @param test[]  The array whose mean is to be found.
 * @param count   The number of elements in the array.
 *
 * @return The mean of the elements of the array.
 */

int find_maximum(unsigned char test[] , int count); 

/**
 * @brief Function to find the maximum number of the array
 *
 * The function takes the array and count of number of elements
 * as input and then iterates through the array using for loop
 * to find the maximum number.
 *
 * @param test[]  The array whose maximum element is to be found.
 * @param count   The number of elements in the array.
 *
 * @return The maximum number of of the array.
 */

int find_minimum(unsigned char test[] , int count); 

/**
 * @brief Function to find the minimum number of the array
 *
 * The function takes the array and count of number of elements
 * as input and then iterates through the array usind the for loop
 * to find the minimum number.
 *
 * @param test[]  The array whose minimum element is to be found.
 * @param count   The number of elements in the array.
 *
 * @return The minimum number of of the array.
 */

int find_median(unsigned char test[] , int count); 

/**
 * @brief Function to find the median of the array
 *
 * The function takes the sorted array and count of number of elements
 * as input and then displays the element of the array at the median position. 
 *
 * @param test[]  The array whose median is to be found.
 * @param count   The number of elements in the array.
 *
 * @return The median of the elements of the array.
 */

void print_stastics(int mean, int median, int maximum, int minimum);

/**
 * @brief Function to print the mean, median, maximum and minimum of the array.
 *
 * The Function takes mean, median, maximum and minimum as input 
 *  from the main and prints them on the screen.
 *
 * @param mean      The mean of the array to be printed
 * @param median    The median of the array to be printed
 * @param maximum   The maximum number of the array to be printed
 * @param minimum   The minimum number of the array to be printed
 */

void print_array(unsigned char test[] , int count); 
/**
 * @brief Function to print the array.
 *
 * The Function takes the array and count of number of elements 
 * as input and prints the array using a for loop. 
 *
 * @param test[]  The array whose median is to be found.
 * @param count   The number of elements in the array.
 */

void sort_array(unsigned char test[], int count);
/**
 * @brief Function to sort the array.
 *
 * The Function takes the array and count of number of elements 
 * as input and sorts the array in descending order using a for loop. 
 *
 * @param test[]  The array whose median is to be found.
 * @param count   The number of elements in the array.
 */


#endif /* __STATS_H__ */
