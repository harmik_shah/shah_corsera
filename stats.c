/******************************************************************************
 * Copyright (C) 2017 by Alex Fosdick - University of Colorado
 *
 * Redistribution, modification or use of this software in source or binary
 * forms is permitted as long as the files maintain this copyright. Users are 
 * permitted to modify this and use it to learn about the field of embedded
 * software. Alex Fosdick and the University of Colorado are not liable for any
 * misuse of this material. 
 *
 *****************************************************************************/
/**
 * @file stats.c 
 * @brief To analyse an array of unsigned character data.
 *
 * To analyse an array of unsigned character data and report 
 * analytics of various functions to find mean, median, maximum 
 * and minimum on the data set.
 *
 * @author Harmik Shah
 * @date 13-04-2020
 *
 */


#include <stdio.h>
#include "stats.h"

int find_mean(unsigned char test[] , int count);
int find_maximum(unsigned char test[] , int count);
int find_minimum(unsigned char test[] , int count);
int find_median(unsigned char test[] , int count);
void sort_array(unsigned char test[], int count);
void print_statistics(int mean, int median, int maximum, int minimum);
void print_array(unsigned char test[], int count);

/* Size of the Data Set */
#define SIZE (40)

void main() {

  unsigned char test[SIZE] = { 34, 201, 190, 154,   8, 194,   2,   6,
                              114, 88,   45,  76, 123,  87,  25,  23,
                              200, 122, 150, 90,   92,  87, 177, 244,
                              201,   6,  12,  60,   8,   2,   5,  67,
                                7,  87, 250, 230,  99,   3, 100,  90};


  /* Other Variable Declarations Go Here */
int Mean;
int Maximum;
int Minimum;
int Median;

  /* Statistics and Printing Functions Go Here */
sort_array(test,SIZE);
Mean = find_mean(test,SIZE);
Maximum = find_maximum(test,SIZE);
Minimum = find_minimum(test,SIZE);
Median = find_median(test,SIZE);
print_array(test,SIZE);
print_statistics(Mean,Median,Maximum,Minimum);

}

/* Add other Implementation File Code Here */
int find_mean(unsigned char test[] , int n){
  int i;
  int avg = 0;
  int mean = 0;
  for(i=0;i<n;i++){
    avg += test[i];
  }
  mean = avg/n;
  return mean;
}
  
void sort_array(unsigned char test[], int n) 
{ 
    int i, j, a; 
  
        for (i = 0; i < n; ++i) 
        {
            for (j = i + 1; j < n; ++j) 
            {
                if (test[i] < test[j]) 
                {
                    a = test[i];
                    test[i] = test[j];
                    test[j] = a;
                }
            }
        }
 
} 
  
int find_maximum(unsigned char test[], int count)
{
    int i ;
    int max=test[0];
    for(i=1;i<count;i++)
    {
      if(test[i]>max)
      {
        max=test[i];

      }
    }
    return max;
}


int find_minimum(unsigned char test[], int count)
{
    int i ;
    int min=test[0];
    for(i=1;i<count;i++)
    {
     if(test[i]<min)
     {
        min=test[i];

      }
    }
    return min;
}

int find_median(unsigned char test[], int count)
{  
  int median = 0;
  count=((count+1)/2)-1;
  median = test[count];
  return median;
}

void print_statistics(int mean, int median, int maximum, int minimum)
{
  printf("\n");
  printf("The maximum is : %d \n",maximum);
  printf("The minimum is : %d \n",minimum);
  printf("The median is : %d \n",median);
  printf("The mean is : %d \n ",mean);
}

void print_array(unsigned char test[], int count)
{
  for(int i=0;i<count;i++)
  {
    printf("%d\t",test[i]);
  }
}
